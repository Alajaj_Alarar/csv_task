<?php

namespace App\Http\Controllers;

use App\User;
use App\user_details;
use Illuminate\Http\Request;

class ContractorsController extends Controller
{
    public function index()
    {
        $users = User::all();
        return view(' contractors')->with('users', $users);
    }

    public function getDetailsInfo($id)
    {
        $user = User::where('Id', $id)->first();
        $gov_id = $user->gov_id;
        $user_details = user_details::where('gov_id', $gov_id)->where('language_id',1)->get();
        return view('details')->with('user_details', $user_details)->with('id', $id);
    }

    public function editdetails($id)
    {
        $user = User::where('Id', $id)->first();
        $gov_id = $user->gov_id;
        $user_details = user_details::where('gov_id', $gov_id)->where('language_id',1)->get();
        return view('editDetails')->with('user_details', $user_details)->with('id', $id);
    }

    public function saveEditDetails(Request $request, $id)
    {
        $inputs = $request->kv;
        $user = User::where('Id', $id)->first();
        $gov_id = $user->gov_id;
        $user_details = user_details::where('gov_id', $gov_id)->where('language_id',1)->get();
        $user_details1 = user_details::where('gov_id', $gov_id)->where('language_id',2)->get();
     
        foreach ($inputs as $key => $value) {
            $temp = $user_details->where('key', $key)->first();
            $temp->value = $value;
            $temp->save();
        }
        foreach ($inputs as $key => $value) {
            $temp = $user_details1->where('key', $key)->first();
            $temp->value = $value;
            $temp->save();
        }
        return redirect()->route('editDetails', ['id' => $id])->with('user_details', $user_details)->with('id', $id);

    }

    public function uploadCsv()
    {
        return view('uploadCsv');
    }
    public function storeCsv(Request $request)
    {
        //get file
        $upload=$request->file('names');
        $filePath=$upload->getRealPath();
        
        //open and read
        header('Content-Type: text/html; charset=UTF-8');
        $file=fopen($filePath, 'r');
        
        $header= fgetcsv($file);
        // dd($header);
        while($columns=fgetcsv($file))
        {
            
            if($columns[0]=="")
            continue;

            $data= array_combine($header, $columns);
            //$data = array_map("utf8_encode", $data);
            
            $exist=User::where('gov_id',$data['gov_id'])->first();
            if($exist)
            continue;
            $user = new User;
            $user->gov_id= $data['gov_id'] ;
            $user->first_name= utf8_encode($data['first_name']);
            $user->father_name = utf8_encode($data['father_name']);
            $user->mother_name= utf8_encode($data['mother_name']);
            $user->last_name= utf8_encode($data['last_name']);
            $user->birth_place = utf8_encode($data['birth_place']);
            $time = strtotime($data['birth_date']);
            $newformat = date('Y-m-d',$time);
            $user->birth_date=  $newformat;            
                   
            $user ->save();
        }
        return redirect()->route('home');
     
    }
     
    public function uploadOtherCsv($id)
    {
       return view('storeOtherCsv')->with('id',$id);
    }

    public function saveOtherCsv (Request $request)
    {
        
        $id=$request->id;
        $user = User::where('Id', $id)->first();
        $gov_id = $user->gov_id;
        $upload=$request->file('names');
        $filePath=$upload->getRealPath();
        
        //open and read
        header('Content-Type: text/html; charset=UTF-8');
        $file=fopen($filePath, 'r');   
        $header= fgetcsv($file);
        $fields=array();
        
        foreach ($header  as $key => $value) {
           array_push($fields,$value);
         
        }
        while($columns=fgetcsv($file))
        {
            if($columns[0]=="")
            continue;
            $data= array_combine($header, $columns);
           
            foreach ($fields as $key => $value) {
                if ($value=="gov_id" or $value==""  )
                continue;
                $user_details = new user_details;
                $user_details1 = new user_details;
                $user_details->gov_id=  utf8_encode($gov_id) ;
                $user_details->key= $value ;
                $user_details->value=  utf8_encode($data[$value]);
                $user_details->language_id=1;
                $user_details->main_id=0 ;
                $user_details->save(); 

                $user_details1->gov_id=  utf8_encode($gov_id) ;
                $user_details1->key= $value ;
                $user_details1->value= utf8_encode($data[$value]);
                $user_details1->language_id=2;
                $user_details1->main_id=$user_details->id ;

               
                $user_details1->save(); 
            }
                   
        }
        return redirect()->route('editDetails', ['id' => $id])->with('user_details', $user_details)->with('id', $id);

    }


}
