<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Storage;


use App\Image;
use App\User;
use Illuminate\Http\Request;

class ImageController extends Controller
{

    public function show($id)
    {$user = User::where('Id', $id)->first();
        $gov_id = $user->gov_id;
        // dd($gov_id);
        $images = Image::where('gov_id', $gov_id)->get();
        return view('ImageUpload')->with('gov_id', $gov_id)->with('images', $images);
    }

    public function store(Request $request)
    {

        $input = $request->all();
        $images = array();
        if ($files = $request->file('images')) {
            foreach ($files as $file) {
                $name = $file->getClientOriginalName();
                $file->move('image', $name);
                Image::create(['gov_id' => $request->gov_id, 'image' => $name]);
            }
        }

        return back()->with('success', 'Image Uploaded successfully.');
    }
    public function destroy($id)
    {
        $image = Image::where('id', $id)->first();

        $image_path = "/image/" . $image->image;
        
      
        
        if (file_exists($image_path)) {
           
            Storage::delete('public'.$image_path);
            Image::where('id', $id)->delete();
        } 
        else
        {
            Image::where('id', $id)->delete();
        }
        return redirect()->back();
    }
}
