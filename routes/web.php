<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('contractors','ContractorsController@index')->name('home');
Route::get('moreDetails/{id}','ContractorsController@getDetailsInfo')->name('editDetails');
Route::get('moreDetails/editDetails/{id}','ContractorsController@editdetails');
Route::post('saveEdit/{id}','ContractorsController@saveEditDetails');

//Upload CSV
Route::get('uploadcsv','ContractorsController@uploadCsv');
Route::post('uploadcsv','ContractorsController@storeCsv');
//Upload CSV
Route::get('uploadOtherCsv/{id}','ContractorsController@uploadOtherCsv')->name('otherInfo');;
Route::post('saveOtherCsv','ContractorsController@saveOtherCsv');


Route::get('upload/{id}','ImageController@show');
Route::post('upload','ImageController@store');
Route::get('upload/deleteImg/{id}','ImageController@destroy');
