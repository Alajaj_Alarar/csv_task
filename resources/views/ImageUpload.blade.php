<!-- Stored in resources/views/child.blade.php -->

@extends('layouts.main')

@section('title', 'تحميل الصورة')


@section('content')

<a class="btn btn-primary"  href="{{route('home')}}">الرجوع </a>
<form action="/upload" class="form-image-upload" method="POST" enctype="multipart/form-data">
    {!! csrf_field() !!}
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if ($message = Session::get('success'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
    </div>
    @endif
    <div class="row">
        <input type="string" name="gov_id" id="gov_id" value=" {{$gov_id}}" hidden >

        <div class="col-md-5 offset-md-3 text-center">
            <strong>Image:</strong>
            <input required type="file" class="form-control" name="images[]" placeholder="address" multiple><br>
            <button type="submit" class="btn btn-success">Upload</button>
        </div>

      

    </div>
</form>
<hr>

@foreach($images->chunk(3) as $chunk)

<div class="row text-center">


        @foreach($chunk as $image)
        <figure class="col-md-4">
        <img  class="offset-md-2" style="border: 1px solid #ddd;border-radius: 4px;padding: 5px;width: 150px;" src="{{ url('/image/'.$image->image) }}" alt="" title="" />
        <br>
        <a class=" btn btn-danger offset-md-2" href="deleteImg/{{$image->id}}">Delete</a>
        </a>
      </figure>

        @endforeach
        </div>
@endforeach
@endsection
