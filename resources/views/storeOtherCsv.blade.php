
@extends('layouts.main')

@section('title', 'بيانات اضافية')


@section('content')

     @if(Session::has('message'))
        <p >{{ Session::get('message') }}</p>
     @endif

     <!-- Form -->
     <form method='post' action='/saveOtherCsv' enctype='multipart/form-data' accept-charset="utf-8">
       {{ csrf_field() }}
       
       <input type="text" name="id" value="{{$id}}" hidden>
       File:<input type='file' name='names' ><br>
       <input type='submit' name='submit' value='Import'>
     </form>

@endsection