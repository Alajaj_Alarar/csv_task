<!-- Stored in resources/views/child.blade.php -->

@extends('layouts.main')

@section('title', 'بيانات المقاول')


@section('content')
<a class="btn btn-primary"  href="{{route('home')}}">الرئيسية </a>
@if(!count($user_details) == 0)
<td><a class="btn btn-primary"  href="editDetails/{{$id}}">تعديل</a></td>
@endif
@if(count($user_details) == 0)
<td><a class="btn btn-primary"  href="{{route('otherInfo', ['id' => $id])}}">اضافة بيانات</a></td>
@endif

<div >



<table class="table table-dark">
  <thead>
    <tr>
      <th scope="col">الحقل</th>
      <th scope="col">القيمة</th>
    </tr>
  </thead>
  <tbody>
  @foreach($user_details as $info)
  
    <tr>

      
      <td><b>{{$info->key}}</b></td>
      <td><b>{{$info->value}}</b></td>

    </tr>
    @endforeach
  </tbody>
</table>
<hr>
</div>

@endsection