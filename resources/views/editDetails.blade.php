@extends('layouts.main')

<h1 class="text-center">ID:{{$id}}</h1>
@section('title', 'بيانات المقاول')
  <form action="/saveEdit/{{$id}}" method="POST" enctype="multipart/form-data">
     {!! csrf_field() !!}   
    @foreach($user_details as $info)

    <div class="form-row text-center">
        <div class="form-group offset-md-4 col-md-3">
          <label for="inputEmail4">{{$info->key}}</label>
         
          <input type="text" name="kv[{{$info->key}}]"  value="{{$info->value}}">
        </div>
    </div>
    @endforeach
    <button type="submit" class="btn btn-success offset-md-4">Upload</button>
  </form>
@section('content')

@endsection