
@extends('layouts.main')

@section('title', 'تحميل الاسماء')


@section('content')

     @if(Session::has('message'))
        <p >{{ Session::get('message') }}</p>
     @endif

     <!-- Form -->
     <form method='post' action='/uploadcsv' enctype='multipart/form-data' accept-charset="utf-8">
       {{ csrf_field() }}
       File:<input type='file' name='names' ><br>
       <input type='submit' name='submit' value='Import'>
     </form>

@endsection