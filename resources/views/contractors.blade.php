<!-- Stored in resources/views/child.blade.php -->

@extends('layouts.main')

@section('title', 'المقاولون')


@section('content')
<h1  class="text-center"> المقاولون</h1
<div class="row">
<a href="uploadcsv"> اضافة مستخدمين</a>
</div>
<div class="row text-center">
>
<table class="table table-dark">
  <thead>
    <tr>
      <th scope="col">المرفقات</th>
      <th scope="col">تفاصيل</th>
      <th scope="col">تاريخ التولد</th>
      <th scope="col">مكان التولد</th>
      <th scope="col">الرقم الوطني</th>
      <th scope="col">الكنية</th>
      <th scope="col">اسم الام</th>
      <th scope="col">اسم الاب</th>
      <th scope="col">الاسم</th>
    </tr>
  </thead>
  <tbody>
  
  @foreach($users as $user)
    <tr>
        <td><a href="/upload/{{$user->id}}" class="btn btn-warning btn-sm active" role="button" aria-pressed="true">عرض المرفقات</a></td>
        <td><a href="/moreDetails/{{$user->id}}" class="btn btn-warning btn-sm active" role="button" aria-pressed="true">تفاصيل</a></td>
        <td>{{$user->birth_date}}</td>
        <td>{{$user->birth_place}}</td>
        <td>{{$user->gov_id}}</td>
        <td>{{$user->last_name}}</td>
        <td>{{$user->mother_name}}</td>
        <td>{{$user->father_name}}</td>
        <td>{{$user->first_name}}</td>
    </tr>
    @endforeach
  </tbody>
</table>
</div>
@endsection